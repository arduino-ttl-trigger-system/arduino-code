# Arduino TTL Trigger Emulator

This project uses an Arduino mega board to emulate TTL triggering. Using an included python script, commands can be sent to the arduino via the command line or a lightweight local webapp depending on preference.


## Installation
With Visual Studio Code, setup a virtual Python environment. A quick way to do this is ctrl+shift+P and running "Python: Create Environment". Choose the Python version you want and have VSCode go through the requirements.txt to have the venv pip automatically pull down necissary packages. 

Note: The Python version in the original venv was 3.11.2. There may be issues with the requirements.txt if you use an older or newer version of Python.

## Usage

### Command Line
When sending commands to the arduino from the command line, use the following format

```python serialPythonScriptLinux.py <start/stop> <clk/rand> <pulse width> <frequency>```

#### `<start/stop>` 
This argument will accept only "start" or "stop". If "start" is the value, the remaining 3 arguments must also be populated. If "stop" is the value, no it should be the only argument.
#### `<clk/rand>`
This argument will accept only "clk" or "rand". If "clk is the value, the trigger will operate in clocked mode, meaning pulses will occur at regular intervals. If "rand" is the value, the trigger will operate in random mode, meaning that there will be a random amount of time in between pulses (distributed in the shape of -ln(x)).
#### `<pulse width>`
This argument denotes the pulse width, in microseconds, of the arduino ON signal.
#### `<frequency>`
This argument indicates frequency (in Hz) for a clocked signal, or approximate center frequency (in Hz) for random signal.

#### Input Vs Actual Frequency
Because of the way the arduino code is written there is some overhead and the actual output frequency has some error. Below is a table of the actual output frequencies corresponding to various input frequencies as measured by Oscilloscope.

| Input (Hz) | Measured Frequency (Hz) |
|------------|-------------------------|
| 10         | 10.09                   |
| 20         | 20.38                   |
| 30         | 30.24                   |
| 40         | 41.55                   |
| 50         | 52.46                   |
| 60         | 62.25                   |
| 70         | 69.49                   |
| 75         | 74.45                   |
| 100        | 99.22                   |
| 125        | 123.97                  |
| 150        | 148.69                  |
| 175        | 173.42                  |
| 200        | 198.09                  |
| 225        | 222.78                  |
| 250        | 247.41                  |
| 275        | 272.06                  |
| 300        | 296.65                  |
| 325        | 321.2                   |
| 350        | 345.78                  |
| 375        | 370.255                 |
| 400        | 394.83                  |
| 425        | 394.83                  |
| 450        | 394.83                  |


### Webapp
cd into the flask folder and run
```flask --app TTL-Emulator.py run```
That it. If you are using Windows, you will need to redirect the import statement in TTL-Emulator.py to serialPythonScriptWin.py

